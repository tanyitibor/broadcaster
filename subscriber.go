package main

import (
	"log"
	"net/http"
	"path/filepath"

	"github.com/gorilla/websocket"
)

type Subscriber struct {
	Client
	Channel     string
	Broadcaster *Broadcaster
}

func NewSubscriber(conn *websocket.Conn, channel string, broadcaster *Broadcaster) *Subscriber {
	s := Subscriber{}
	s.conn = conn
	s.Broadcaster = broadcaster
	s.send = make(chan []byte)
	s.Channel = channel

	go s.waitWrite()
	go s.waitClose()

	return &s
}

func (s *Subscriber) waitClose() {
	s.waitRead()
	s.Broadcaster.unsubscribe <- s
}

func (app *App) handleSubscriberConnect(w http.ResponseWriter, r *http.Request) {
	if !websocket.IsWebSocketUpgrade(r) {
		log.Printf("Not websocket connection")
		return
	}

	channel := r.URL.Query().Get("channel")
	if channel == "" {
		log.Print("No channel defined")
		return
	}

	if ok, err := filepath.Match(app.env["REDIS_CHANNEL"], channel); !ok || err != nil {
		log.Printf("Invalid channel name '%v'", channel)
		return
	}

	if !app.auth(r) {
		return
	}

	conn, err := app.websocketUpgrade(w, r)
	if err != nil {
		log.Print("Websocket upgrade error")
		return
	}

	app.broadcaster.subscribe <- NewSubscriber(conn, channel, app.broadcaster)
}
