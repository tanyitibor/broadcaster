FROM scratch

ADD cmd/broadcaster /
ADD cmd/.env /

CMD ["/broadcaster"]