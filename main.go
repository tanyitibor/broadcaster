package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

type App struct {
	broadcaster *Broadcaster
	router      *chi.Mux
	env         ENV
	database    *DatabaseService
}

func NewApp() *App {
	app := &App{
		broadcaster: NewBroadcaster(),
		router:      chi.NewMux(),
		env:         loadEnv(),
	}

	if app.env["DB_ENABLED"] == "true" {
		mongodb := NewMongoDB(app.env["DB_URL"], app.env["DB_DATABASE"], app.env["DB_TABLE"])
		app.database = NewDatabaseService(mongodb)
		go app.database.Run()
	}

	app.router.HandleFunc("/sub", app.handleSubscriberConnect)

	if app.env["HTTP_MESSAGING"] == "true" {
		app.router.Post("/msg", app.handleHTTPMsg)
	}

	if app.env["REDIS_ENABLED"] == "true" {
		go app.waitRedis()
	}

	return app
}

func main() {
	app := NewApp()

	log.Println("Server started.")

	log.Fatal(http.ListenAndServe(":"+app.env["PORT"], app.router))
}
