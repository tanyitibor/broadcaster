package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

func (app *App) websocketUpgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	upgrader := &websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			if app.env["HOST"] != "" && app.env["HOST"] != r.Host {
				log.Printf("Invalid host %s", r.Host)

				return false
			}

			return true
		},
	}

	return upgrader.Upgrade(w, r, nil)
}
