package main

import (
	"errors"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait = 5 * time.Second
)

type Client struct {
	conn *websocket.Conn
	send chan []byte
}

func (c *Client) waitWrite() {
	defer c.conn.Close()

	for {
		message, ok := <-c.send
		c.conn.SetWriteDeadline(time.Now().Add(writeWait))
		if !ok {
			c.conn.WriteMessage(websocket.CloseMessage, []byte{})
			break
		}

		if c.sendMsg(message) != nil {
			break
		}
	}
}

func (c *Client) waitRead() {
	for {
		_, _, err := c.conn.ReadMessage()
		if err != nil {
			c.conn.Close()
			break
		}
	}
}

func (c *Client) sendMsg(msg []byte) error {
	w, err := c.conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return errors.New("Writer error")
	}

	_, err = w.Write(msg)
	if err != nil {
		return errors.New("Write error")
	}

	return w.Close()
}
