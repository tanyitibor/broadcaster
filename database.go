package main

import (
	"errors"
	"log"
)

type Database interface {
	Insert(data interface{}) error
}

type DatabaseService struct {
	database Database
	send     chan interface{}
}

func NewDatabaseService(database Database) *DatabaseService {
	send := make(chan interface{})

	return &DatabaseService{database, send}
}

func (s *DatabaseService) Run() {
	for {
		data, ok := <-s.send
		if !ok {
			log.Print(errors.New("mongoservice send error"))
		}

		err := s.database.Insert(data)
		if err != nil {
			log.Print(err)
		}
	}
}
