package main

import (
	"encoding/json"
	"net/http"
)

func (app *App) handleHTTPMsg(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	if r.Body == nil {
		http.Error(w, "No request body", http.StatusBadRequest)
		return
	}

	var msg BroadCastMessage
	err := json.NewDecoder(r.Body).Decode(&msg)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	if app.env["DATABASE_ENABLED"] == "true" {
		app.database.send <- msg
	}
	app.broadcaster.broadcast <- msg

	w.Write([]byte("ok"))
}
