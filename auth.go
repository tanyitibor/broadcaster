package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

var httpClient = http.Client{
	Timeout: time.Second * 5,
}

func (app *App) auth(r *http.Request) bool {
	if app.env["AUTH_URL"] == "" {
		return true
	}

	json, _ := json.Marshal(r.URL.Query())
	reader := bytes.NewReader(json)

	_, err := httpClient.Post(app.env["AUTH_URL"], "application/json", reader)

	return err != nil
}
