package main

type Broadcaster struct {
	subscribers map[*Subscriber]bool
	broadcast   chan BroadCastMessage
	subscribe   chan *Subscriber
	unsubscribe chan *Subscriber
}

func NewBroadcaster() *Broadcaster {
	b := &Broadcaster{
		subscribers: map[*Subscriber]bool{},
		broadcast:   make(chan BroadCastMessage),
		subscribe:   make(chan *Subscriber),
		unsubscribe: make(chan *Subscriber),
	}

	go b.await()

	return b
}

type BroadCastMessage struct {
	Channel string `json:"channel"`
	Message string `json:"message"`
}

func (b *Broadcaster) await() {
	for {
		select {
		case msg := <-b.broadcast:
			for s := range b.subscribers {
				if s.Channel == msg.Channel {
					s.send <- []byte(msg.Message)
				}
			}

		case s := <-b.subscribe:
			b.subscribers[s] = true

		case u := <-b.unsubscribe:
			if _, ok := b.subscribers[u]; ok {
				delete(b.subscribers, u)
				close(u.send)
			}
		}
	}
}
