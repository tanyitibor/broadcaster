package main

import (
	"flag"
	"log"

	"github.com/joho/godotenv"
)

type ENV map[string]string

var defaultEnv = ENV{
	"PORT":           "8080",
	"HOST":           "",
	"HTTP_MESSAGING": "false",
	"REDIS_ENABLED":  "true",
	"REDIS_URL":      "localhost:6379",
	"REDIS_CHANNEL":  "channels.*",
	"AUTH_URL":       "",
	"DB_ENABLED":     "false",
	"DB_URL":         "localhost:27017",
	"DB_DATABASE":    "test",
	"DB_TABLE":       "test",
}

func loadEnv() ENV {
	loaded, err := godotenv.Read()
	if err != nil {
		log.Print(err)

		return defaultEnv
	}

	env := ENV{}
	for k, v := range defaultEnv {
		newVal, ok := loaded[k]
		if !ok {
			env[k] = v
			continue
		}

		env[k] = newVal
	}

	flags := parseFlags()
	flag.CommandLine.Visit(func(fl *flag.Flag) {
		env[fl.Name] = *flags[fl.Name]
	})

	return env
}

func parseFlags() map[string]*string {
	flags := map[string]*string{}

	for k, v := range defaultEnv {
		flags[k] = flag.String(k, v, "")
	}

	flag.Parse()

	return flags
}
