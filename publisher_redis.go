package main

import (
	"log"

	"github.com/go-redis/redis"
)

func (app *App) waitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr: app.env["REDIS_URL"],
	})

	_, err := client.Ping().Result()
	if err != nil {
		log.Fatalf("Redis error on %v", app.env["REDIS_URL"])
	}

	ps := client.PSubscribe(app.env["REDIS_CHANNEL"])
	for {
		msg := <-ps.Channel()

		bMsg := BroadCastMessage{
			Channel: msg.Channel,
			Message: msg.Payload,
		}

		app.broadcaster.broadcast <- bMsg
	}
}
