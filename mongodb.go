package main

import (
	"log"

	"gopkg.in/mgo.v2"
)

type MongoDB struct {
	collection *mgo.Collection
}

func NewMongoDB(connString string, dbName string, collectionName string) *MongoDB {
	session, err := mgo.Dial(connString)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to MongoDB on", connString)

	collection := session.DB(dbName).C(collectionName)

	return &MongoDB{collection}
}

func (db *MongoDB) Insert(data interface{}) error {
	return db.collection.Insert(data)
}
